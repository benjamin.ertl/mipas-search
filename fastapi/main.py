import os
import re
import glob
import uuid
import tarfile
import base64
import shutil

from pathlib import Path
from functools import lru_cache

from fastapi import FastAPI, Form, Request
from fastapi.responses import HTMLResponse, FileResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from typing import Union
from pydantic import BaseSettings


class Settings(BaseSettings):
    data_path: str = "data"


settings = Settings()
app = FastAPI()

#app.mount("/data", StaticFiles(directory=settings.data_path), name="data")
#app.mount("/archives", StaticFiles(directory="archives"), name="archives")
templates = Jinja2Templates(directory="templates")


@lru_cache(maxsize=16)
def search_files(search_string: str):
    p = re.compile(search_string)

    file_list = sorted(glob.glob(f"{settings.data_path}/**/*",recursive=True))
    file_list = [f for f in file_list if p.match(f)]

    return file_list


@app.get("/", response_class=HTMLResponse)
async def root(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})


@app.post("/search", response_class=HTMLResponse)
async def search(request: Request,
    year: int = Form(None), month: int = Form(None), day: int = Form(None),
    gas: str = Form(None), spectra_version: str = Form(None),
    baseline_version: int = Form(None), submit: str = Form()
):

    data_type = "nc"
    
    year = year if year else '\d{4}'
    month = f"{month:02d}" if month else '\d{2}'
    # day = f"{day:02d}" if day else '\d{0,2}'
    day = '\d{0,2}'
    gas = gas if gas else '.*'
    spectra_version = spectra_version if spectra_version else '.*'
    baseline_version = baseline_version if baseline_version else '.*'

    
    search_string = f".*/MIPAS-E_IMK\.{year}{month}{day}\.{spectra_version}_{gas}_{baseline_version}\.{data_type}"
    file_list = search_files(search_string)

    #files = [{"link": f.replace(settings.data_path,"/data"), "name": os.path.basename(f)} for f in file_list]
    files = [{"link": "file/" + base64.urlsafe_b64encode(bytes(f,'utf-8')).decode('utf-8'),
              "name": os.path.basename(f)} for f in file_list]

    download_link = f"download/?year={year}&month={month}&day={day}"
    download_link += f"&spectra_version={spectra_version}&gas={gas}"
    download_link += f"&baseline_version={baseline_version}&data_type={data_type}"
    
    return templates.TemplateResponse("result.html", {
        "request": request,
        "search_string": search_string,
        "files": files,
        "download_link": download_link,
    })


@app.get("/file/{file_name}", response_class=RedirectResponse)
async def download_file(file_name: str):
    file_path = base64.urlsafe_b64decode(file_name).decode('utf-8')
    target_path = os.path.join("/", "var", "www", "html", "download", os.path.basename(file_path))
    #file_ = Path(file_path).resolve()
    shutil.copyfile(file_path, target_path)
    #return {"file_path": file_path, "target_path": target_path}
    return f"https://imk-asf-mipas.imk.kit.edu/download/{os.path.basename(file_path)}"

    
#@app.get("/download/", response_class=FileResponse)
@app.get("/download/", response_class=RedirectResponse)
async def download(year: str, month: str, day: str, spectra_version: str,
    gas: str, baseline_version: str, data_type: str
):
    search_string = f".*/MIPAS-E_IMK\.{year}{month}{day}\.{spectra_version}_{gas}_{baseline_version}\.{data_type}"
    file_list = search_files(search_string)
    file_list = map(lambda x: Path(x).resolve(), file_list)

    rnd_file_name = f"{str(uuid.uuid1())}.tar.gz"
    tar_file_name = os.path.join("/", "var", "www", "html", "download", rnd_file_name)

    tar_file = tarfile.open(tar_file_name,"w:gz")
    [tar_file.add(f,arcname=os.path.basename(f)) for f in file_list]
    tar_file.close()

    #return FileResponse(tar_file_name, filename=os.path.basename(tar_file_name))
    return f"https://imk-asf-mipas.imk.kit.edu/download/{rnd_file_name}"
